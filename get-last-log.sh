#!/bin/bash
WORKDIR=$(pwd)
source $WORKDIR/util/error.sh

filename=$1
if [ ! -z $filename ]
then
	tail -f -n 1 $(latest "$WORKDIR/logs" $filename)
else
	tail -f -n 1 $(latest "$WORKDIR/logs" $filename)
fi

