# Bash VS Code Extension Pack

Opinionated extension pack to improve Bash usage

## Requirements

### Shell Format

To enjoy the [shell-format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format) extension, [`shfmt`](https://github.com/mvdan/sh) must be installed

### DirEnv

To enjoy the [direnv](https://marketplace.visualstudio.com/items?itemName=cab404.vscode-direnv) extension, follow the [direnv](https://direnv.net/) installation procedure

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
