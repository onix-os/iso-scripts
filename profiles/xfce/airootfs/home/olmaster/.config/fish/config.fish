if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -gx GOPATH $HOME/Go
set -gx PATH $PATH $HOME/.local/bin $GOPATH/bin $HOME/.config/composer/vendor/bin $HOME/Bin
set -gx MSF_DATABASE_CONFIG "$HOME/.msf4/database.yml"

