package image

import (
	"fmt"
	"image"

	"gitlab.com/olanguage/olang/color"
	"gitlab.com/olanguage/olang/debug"
	"gitlab.com/olanguage/olang/file"
)

type Image struct {
	Name    string
	Size    int
	Type    string
	Content []byte
	Decoded image.Image
}

func New(dataOrFile interface{}) *Image {
	switch d := dataOrFile.(type) {
	case string:
		imageFile := file.New(d)
		imageDecode := image.Decode(imageFile.Content)
		return &Image{
			Name:    imageFile.Name,
			Size:    imageFile.Size,
			Type:    imageFile.Type,
			Content: imageFile.Content,
			Decoded: imageDecode
		}
		break
	case []byte:
		imageDecode := image.Decode(dataOrFile)
		return &Image{
			Content: d.Content,
			Decoded: imageDecode
		}
		break
	default:
		errorType(dataOrFile)
	}
}

func errorType(givenType interface{}) {
	debugger := debug.New()
	fmt.Printf("Error ", color.Bold(color.Red(debugger.SprintInterface(givenType))))
}
