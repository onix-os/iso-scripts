package types

/*
 *
 * Types as convert or define types automaticaly
 *
 * def d = 1; #int
 *
 * int d = 1; #int
 *
 * float d = 1.5; #float
 *
 * bool d = true;
 *
 *
 * byte d = "test";
 *
 *
 * string d = "test";
 *
 * int d = fn(d){ return d };
 */

type Type struct {
	Type  string
	Value interface{}
}

type Result struct {
	Value interface{}
}

var types = map[string]interface{}{
	"Int64":   "int64",
	"Int32":   "int32",
	"Int":     "int",
	"Float64": "float64",
	"Float32": "float32",
	"Float":   "float",
	"String":  "string",
	"Byte":    "byte",
	"Boolean": "bool",
}

func (t *Type) Check() bool {
	for ty := range types {
		if ty == t.Type {
			return true
		}
	}
	return false
}

func (t *Type) Convert() *Result {
	res := &Result{Value: nil}
	if t.Check() {
		switch t.Type {
		case "int64":
			res.Value = t.Value.(int64)
		case "int32":
			res.Value = t.Value.(int32)
		case "int":
			res.Value = t.Value.(int64)
		case "float64":
			res.Value = t.Value.(float64)
		case "float32":
			res.Value = t.Value.(float32)
		case "float":
			res.Value = t.Value.(float64)
		case "string":
			res.Value = t.Value.(string)
		case "byte":
			res.Value = t.Value.(byte)
		case "bool":
			res.Value = t.Value.(bool)
		}
	}

	return res
}
