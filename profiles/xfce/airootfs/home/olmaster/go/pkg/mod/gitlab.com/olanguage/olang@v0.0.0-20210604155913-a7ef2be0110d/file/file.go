package file

import (
	"fmt"
	"os"
)

const (
	BYTE = 1024
	KB   = (BYTE * BYTE)
	MB   = (KB * BYTE)
	GB   = (MB * BYTE)
	TB   = (GB * BYTE)
	PB   = (TB * BYTE)
)

type File struct {
	Name      string
	Size      int
	ReadBytes int
	Type      string
	Path      string
	Content   []byte
	Splited   [][]byte
}

func New(path string) *File {
	return &File{
		Path: path,
	}
}

func (f *File) Open() *File {
	file, err := os.Open(f.Path)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		fmt.Println(err)
	}

	f.Size = int(fileinfo.Size())

	buffer := make([]byte, f.Size)

	bytesread, err := file.Read(buffer)
	if err != nil {
		fmt.Println(err)
	}

	f.Content = buffer
	f.ReadBytes = bytesread

	return f
}

func splitChunks(buf []byte, lim int) [][]byte {
	var chunk []byte
	chunks := make([][]byte, 0, len(buf)/lim+1)
	for len(buf) >= lim {
		chunk, buf = buf[:lim], buf[lim:]
		chunks = append(chunks, chunk)
	}
	if len(buf) > 0 {
		chunks = append(chunks, buf[:len(buf)])
	}
	return chunks
}

func joinChunks(chunks [][]byte) []byte {
	var result []byte
	for len(chunks) > 0 {
		for _, v := range chunks {
			for _, d := range v {
				result = append(result, d)
			}
		}
	}
	return result
}

func (f *File) Split(max int, maxType string) *File {
	divideByte := 0
	switch maxType {
	case "BYTE":
		divideByte = max * BYTE
	case "KB":
		divideByte = max * KB
	case "MB":
		divideByte = max * MB
	case "GB":
		divideByte = max * GB
	case "TB":
		divideByte = max * TB
	case "PB":
		divideByte = max * PB
	default:
		divideByte = max * KB
	}

	f.Splited = splitChunks(f.Content, divideByte)
	f.Content = []byte{}
	return f
}

func (f *File) Join() *File {
	f.Content = joinChunks(f.Splited)
	return f
}
