package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"runtime"
	"sort"

	"github.com/urfave/cli"
	"gitlab.com/olanguage/olang/color"
	"gitlab.com/olanguage/olang/olpfile"
	"gitlab.com/olanguage/olang/opsfile"
	"gitlab.com/olanguage/olang/repl"
)

var debug bool
var live bool

func getGOMAXPROCS() int {
	return runtime.GOMAXPROCS(0)
}

func main() {

	procFile := "proc.ops"
	packageFile := "library.olp"

	app := &cli.App{
		Name:    "olang",
		Usage:   "O Language programming language!",
		Version: "5.0",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "file",
				Usage: "Run O Language File",
			},
			&cli.StringFlag{
				Name:  "debug",
				Usage: "Enable Debugging",
				Value: "false",
			},
			&cli.StringFlag{
				Name:  "live",
				Usage: "Enable Live File Watcher",
				Value: "false",
			},
			&cli.IntFlag{
				Name:  "proc",
				Usage: "Maximum Proccess",
				Value: 0,
			},
		},
		Action: func(c *cli.Context) error {
			run := c.String("file")

			maxproc := getGOMAXPROCS()

			if c.Int("maxproc") > 0 {
				maxproc = c.Int("maxproc")
			}

			runtime.GOMAXPROCS(maxproc)

			debug = false
			live = false

			if c.String("debug") == "true" {
				debug = true
			}

			if c.String("live") == "true" {
				live = true
			}

			if len(run) > 0 {
				filename := run

				repl.FileParse(filename, os.Stdout, debug, live)
			} else {
				if c.NArg() > 0 {
					if c.Args().First() != "" {
						repl.FileParse(c.Args().Get(0), os.Stdout, debug, live)
					}
				} else {
					user, err := user.Current()
					if err != nil {
						log.Fatal(err)
					}
					fmt.Printf("Hello %s! This is the Olang programming language!\n", user.Username)
					repl.Start(os.Stdin, os.Stdout, debug)
				}
			}
			return nil
		},
		Commands: []cli.Command{
			{
				Name:    "run",
				Aliases: []string{"r"},
				Usage:   "Run O Language File ",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file",
						Usage: "Run Olang File",
					},
					&cli.StringFlag{
						Name:  "debug",
						Usage: "Enable Debugging",
						Value: "false",
					},
				},
				Action: func(c *cli.Context) error {
					run := c.String("file")

					maxproc := getGOMAXPROCS()

					if c.Int("maxproc") > 0 {
						maxproc = c.Int("maxproc")
					}

					runtime.GOMAXPROCS(maxproc)

					debug = false

					if c.String("debug") == "true" {
						debug = true
					}

					if len(run) > 0 {
						filename := run
						repl.FileParse(filename, os.Stdout, debug, live)
					} else {
						if c.NArg() > 0 {
							if c.Args().First() != "" {
								repl.FileParse(c.Args().Get(0), os.Stdout, debug, live)
							}
						}
					}
					return nil
				},
			},
			{
				Name:    "exec",
				Aliases: []string{"e"},
				Usage:   "Exec O Language Text ",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "debug",
						Usage: "Enable Debugging",
						Value: "false",
					},
				},
				Action: func(c *cli.Context) error {
					maxproc := getGOMAXPROCS()

					if c.Int("maxproc") > 0 {
						maxproc = c.Int("maxproc")
					}

					runtime.GOMAXPROCS(maxproc)

					debug = false

					if c.String("debug") == "true" {
						debug = true
					}

					if c.NArg() > 0 {
						if c.Args().First() != "" {
							repl.StringParse(c.Args().Get(0), os.Stdout, debug)
						}
					}
					return nil
				},
			},
			{
				Name:    "compile",
				Aliases: []string{"c"},
				Usage:   "Compile O Language Code ",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file, f",
						Usage: "Run Olang File",
					},
				},
				Action: func(c *cli.Context) error {
					run := c.String("file")

					maxproc := getGOMAXPROCS()

					if c.Int("maxproc") > 0 {
						maxproc = c.Int("maxproc")
					}

					runtime.GOMAXPROCS(maxproc)

					debug = false

					if c.String("debug") == "true" {
						debug = true
					}

					if len(run) > 0 {
						filename := run
						repl.FileCompile(filename, os.Stdout, debug)
					} else {
						if c.NArg() > 0 {
							if c.Args().First() != "" {
								repl.FileCompile(c.Args().Get(0), os.Stdout, debug)
							}
						}
					}
					return nil
				},
			},
			{
				Name:    "install",
				Aliases: []string{"i"},
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file, f",
						Usage: ".olp File",
						Value: packageFile,
					},
				},
				Usage: "Install all depends",
				Action: func(c *cli.Context) error {
					packFile := c.String("file")
					olp := olpfile.New(packFile, "vendor")
					olp.Install()
					return nil
				},
			},
			{
				Name:    "generate",
				Aliases: []string{"g"},
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file, f",
						Usage: ".olp File",
						Value: packageFile,
					},
				},
				Usage: "Generate olpfile.json for libraries",
				Action: func(c *cli.Context) error {
					packFile := c.String("file")
					olp := olpfile.New(packFile, "vendor")
					olp.CreateFile()
					return nil
				},
			},
			{
				Name:    "build",
				Aliases: []string{"b"},
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file, f",
						Usage: ".olp File",
						Value: packageFile,
					},
				},
				Usage: "Build project",
				Action: func(c *cli.Context) error {
					packFile := c.String("file")
					olp := olpfile.New(packFile, "vendor")

					if olpfile.Exists("vendor/ondle.ola") {
						parsedOlp := olp.Parse()

						olp.CompileProject(parsedOlp.Main, parsedOlp.Compiled+"/", parsedOlp.Name)
					}
					return nil
				},
			},
			{
				Name:    "get",
				Aliases: []string{"g"},
				Usage:   "Get sources to this folder",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "file, f",
						Usage: ".olp File",
						Value: packageFile,
					},
				},
				Action: func(c *cli.Context) error {
					packFile := c.String("file")
					olp := olpfile.New(packFile, "vendor")

					parsed := olp.Parse()

					userEnter := olpfile.Require{Name: c.Args().Get(0), Src: c.Args().Get(1)}

					parsed.Requires = append(parsed.Requires, userEnter)

					olp.WriteOlpFile(parsed)
					olp.Install()

					return nil
				},
			},
			{
				Name:    "job",
				Aliases: []string{"j"},
				Usage:   "Run jobs in project",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "name",
						Usage: "Run job name",
					},
					&cli.StringFlag{
						Name:  "file, f",
						Usage: ".olp File",
						Value: packageFile,
					},
				},
				Action: func(c *cli.Context) error {
					olp := olpfile.New(packageFile, "vendor")
					jobname := c.Args().Get(0)

					if jobname != "" {
						olp.JobRun(jobname)
					} else {
						jobname = c.String("name")

						if jobname != "" {
							olp.JobRun(jobname)
						}
					}

					return nil
				},
			},
			{
				Name:    "process",
				Aliases: []string{"p"},
				Usage:   "Start olang proccess from Opsfile",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "start, s",
						Usage: "Start .ops Proccess",
						Value: "false",
					},
					&cli.StringFlag{
						Name:  "stop, s",
						Usage: "Stop .ops Proccess",
						Value: "false",
					},
					&cli.StringFlag{
						Name:  "init, i",
						Usage: "Init .ops File",
						Value: "false",
					},
					&cli.StringFlag{
						Name:  "file, i",
						Usage: ".ops Filename",
						Value: procFile,
					},
				},
				Action: func(c *cli.Context) error {
					start := c.String("start")
					stop := c.String("stop")
					init := c.String("init")
					file := c.String("file")

					wd, _ := os.Getwd()

					if _, err := os.Stat(wd + "/" + file); os.IsNotExist(err) {
						log.Fatal(err)
					}

					runable := opsfile.New(file)

					if start == "true" {
						runable.Run()
						os.Exit(1)
					}

					if stop == "true" {
						runable.Kill()
						os.Exit(1)
					}

					if init == "true" {
						by := []byte("[olang]\nprocname=boot.ola\n")
						err := ioutil.WriteFile(file, by, 0777)
						if err != nil {
							log.Fatal(err)
						}
						os.Exit(1)
					}

					return nil
				},
			},
		},
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	err := app.Run(os.Args)
	if err != nil {
		printParserError(os.Stdout, err)
	}
}

func printParserError(out io.Writer, err error) {
	io.WriteString(out, color.Bold(color.Red("File Compile/Run Unsuccessfull!\n")))
	io.WriteString(out, color.Bold(color.Blue("  syntax errors:\n")))

	io.WriteString(out, "\t"+fmt.Sprintf("%s", err)+"\n")
}
