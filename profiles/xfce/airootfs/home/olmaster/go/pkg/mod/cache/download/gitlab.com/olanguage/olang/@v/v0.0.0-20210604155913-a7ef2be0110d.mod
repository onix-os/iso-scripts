module gitlab.com/olanguage/olang

go 1.16

require (
	github.com/buger/jsonparser v1.1.1
	github.com/clbanning/mxj v1.8.4
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.10.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-gonic/gin v1.7.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.2
	github.com/mattn/go-colorable v0.1.8
	github.com/mattn/go-isatty v0.0.13
	github.com/mxk/go-sqlite v0.0.0-20140611214908-167da9432e1f
	github.com/oytunistrator/asm v0.0.0-20180810134229-fdb6eaeba5f8
	github.com/satori/go.uuid v1.2.0
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/syndtr/goleveldb v1.0.0
	github.com/urfave/cli v1.22.5
	gitlab.com/olanguage/ops v0.0.0-20181204092000-e82b6c971e09
	gopkg.in/ini.v1 v1.62.0
	gopkg.in/src-d/go-git.v4 v4.13.1
)
