package binary

/*
Convert assembly code to binary

assembly string -> binary -> write binary file
*/

import (
	"encoding/binary"
	"fmt"
	"time"
	"bytes"
	"gitlab.com/olanguage/olang/object"
	stub "github.com/oytunistrator/asm/generator/unsafe-stub"
	"gitlab.com/olanguage/olang/opcode"
)

/*
TODO: get all string parse all assembly to byte code / test code with unsafe stub
*/

type Binary struct{
	Assembly string
	Content bytes.Buffer
	Opcode opcode.Opcode
}

func New(asm string) *Binary{
	bin := &Binary{
		Assembly: asm
	}
	return bin
}

func (*b Binary) convert() {
	
}

func (*b Binary) test() error{

}