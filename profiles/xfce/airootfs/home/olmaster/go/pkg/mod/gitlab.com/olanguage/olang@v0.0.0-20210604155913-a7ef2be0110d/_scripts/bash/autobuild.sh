#!/bin/bash
export WORKDIR=$(pwd)
cd ../..

clear
echo "Updating..."
go get -u -v gitlab.com/olanguage/olang
go get -u -v gitlab.com/olanguage/ops
go get -u -v gitlab.com/olanguage/opkg

clear
echo "Building..."
if [ ! -d build ];then
    mkdir build
fi

cd build
go build -v ..
cd ..
clear

echo "Done."