#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="onixos-xfce"
iso_label="ONIX"
iso_publisher="Oytun Ozdemir <oytunozdemir@yandex.com>"
iso_application="OnixOS XFCE Edition"
iso_version="$(date +%y%m%d%H%M)"
install_dir="onixos"
#bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
