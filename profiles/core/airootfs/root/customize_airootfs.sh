#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

#systemctl set-default graphical.target
#systemctl enable slim
systemctl enable NetworkManager

groupadd olmaster

useradd --create-home -m -g olmaster -G root -s /bin/bash olmaster 
usermod --password $(openssl passwd olmaster) olmaster
usermod --password $(openssl passwd toor) root
usermod -s /usr/bin/zsh olmaster

cp -aT /etc/skel/ /home/olmaster/
chown -Rv olmaster:olmaster /home/olmaster/
chmod 775 -Rv /home/olmaster/

export HPATH=$(runuser -l olmaster -c "echo $PATH")

echo "export GOPATH=/home/olmaster/go" >> /home/olmaster/.bashrc
echo "export PATH=$HPATH:/home/olmaster/go/bin" >> /home/olmaster/.bashrc
echo "export GOPATH=/home/olmaster/go" >> /home/olmaster/.zshrc
echo "export PATH=$HPATH:/home/olmaster/go/bin" >> /home/olmaster/.zshrc

echo "olmaster ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

cat /etc/os-release > /usr/lib/os-release

echo "[Icon Theme]" > /usr/share/icons/default/index.theme
echo "Inherits=core" >> /usr/share/icons/default/index.theme

