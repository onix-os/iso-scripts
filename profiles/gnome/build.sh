#!/usr/bin/env bash

_buildsh_path="$(realpath -- "$0")"
WORKDIR=$(pwd)
mkdir -p build
exec mkarchiso -C "$WORKDIR/pacman.conf" -v -w build/work -o build/out -L "ONIXOS" "$@" "${_buildsh_path%/*}"
