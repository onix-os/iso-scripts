#!/bin/bash
export WORKDIR=$(pwd)
cd ../..

go get -v github.com/mitchellh/gox

if [ ! -d build/ ]
then
    mkdir build/
fi

cd build/
gox ..
cd ..
