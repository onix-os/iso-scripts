package repl

import (
	"io"

	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"gitlab.com/olanguage/olang/color"
	"gitlab.com/olanguage/olang/compiler"
	"gitlab.com/olanguage/olang/evaluator"
	"gitlab.com/olanguage/olang/lexer"
	"gitlab.com/olanguage/olang/object"
	"gitlab.com/olanguage/olang/parser"
	"gitlab.com/olanguage/olang/watcher"
)

const PROMPT = "olang> "

func check(out io.Writer, e error) {
	if e != nil {
		printParserError(out, e)
	}
}

func FileCompile(filename string, out io.Writer, debug bool) {
	env := object.NewEnvironment()
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		printParserError(out, err)
	}

	contstr := string(content)

	if len(contstr) > 0 {
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$

		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)

		loads := rel.FindAllString(contstr, -2)

		loaded := ""
		for _, load := range loads {
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			if debug {
				fmt.Printf("Loading: %s\n", load)
			}
			curDir, _ := os.Getwd()
			loadedContent, err := ioutil.ReadFile(curDir + "/" + load)
			if err != nil {
				printParserError(out, err)
			}
			loaded += string(loadedContent)
		}

		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "$1W")
		contstr = strings.Replace(contstr, "\n", " ", -2)
		contstr = strings.Replace(contstr, "\t", " ", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			if debug {
				fmt.Printf("Stoped Line:\n")
				fmt.Printf("%#v\n", p.LexerReturn())
				fmt.Printf("%#v\n", p.ErrorTokens())
			}
			return
		}

		compiledBinary := compiler.Eval(program, env)

		f, _ := os.Create(filename + ".bin")
		f.WriteString(compiledBinary.Result())
		f.Sync()
		os.Chmod(filename+".bin", 0777)
	}
}

func StringParse(input string, out io.Writer, debug bool) {
	env := object.NewEnvironment()

	contstr := string(input)

	if len(contstr) > 0 {
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$

		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)

		loads := rel.FindAllString(contstr, -2)

		loaded := ""
		for _, load := range loads {
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			if debug == true {
				fmt.Printf("Loading: %s\n", load)
			}
			curDir, _ := os.Getwd()
			loadedContent, err := ioutil.ReadFile(curDir + "/" + load)
			if err != nil {
				printParserError(out, err)
			}
			loaded += string(loadedContent)
		}

		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "$1W")
		contstr = strings.Replace(contstr, "\n", " ", -2)
		contstr = strings.Replace(contstr, "\t", " ", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			if debug == true {
				fmt.Printf("Stoped Line:\n")
				fmt.Printf("%#v\n", p.LexerReturn())
			}
			return
		}

		evaluated := evaluator.Eval(program, env)

		if evaluated != nil {
			if evaluated.Result() != "" {
				io.WriteString(out, evaluated.Result())
				if debug == true {
					fmt.Printf("\n%#v\n", evaluated)
				}
			}
		}
	}
}

func FileParse(filename string, out io.Writer, debug bool, live bool) {
	env := object.NewEnvironment()
	content, err := ioutil.ReadFile(filename)
	w := watcher.New()
	if err != nil {
		printParserError(out, err)
	}

	if live {
		w.Add(filename)
	}

	contstr := string(content)

	if len(contstr) > 0 {
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$

		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)

		loads := rel.FindAllString(contstr, -2)

		loaded := ""
		for _, load := range loads {
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			if debug == true {
				fmt.Printf("Loading: %s\n", load)
			}
			if live {
				w.Add(load)
			}
			curDir, _ := os.Getwd()
			loadedContent, err := ioutil.ReadFile(curDir + "/" + load)
			if err != nil {
				printParserError(out, err)
			}
			loaded += string(loadedContent)
		}

		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "$1W")
		contstr = strings.Replace(contstr, "\n", " ", -2)
		contstr = strings.Replace(contstr, "\t", " ", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			if debug == true {
				fmt.Printf("Stoped Line:\n")
				fmt.Printf("%#v\n", p.LexerReturn())
				fmt.Printf("%#v\n", p.ErrorTokens())
			}
			return
		}

		evaluated := evaluator.Eval(program, env)

		if evaluated != nil {
			if evaluated.Result() != "" {
				io.WriteString(out, evaluated.Result())
				if debug == true {
					fmt.Printf("\n%#v\n", evaluated)
				}
			}
		}

		if live {
			w.Run(func() {
				FileParse(filename, out, debug, live)
			})
		}

		return
	}
}

func printParserError(out io.Writer, err error) {
	io.WriteString(out, color.Bold(color.Red("File Compile/Run Unsuccessfully!\n")))
	io.WriteString(out, color.Bold(color.Blue("  syntax errors:\n")))

	io.WriteString(out, "\t"+fmt.Sprintf("%s", err)+"\n")

	return
}

func printParserErrors(out io.Writer, errors []string) {
	io.WriteString(out, color.Bold(color.Red("File Compile/Run Unsuccessfull!\n")))
	io.WriteString(out, color.Bold(color.Blue("  syntax errors:\n")))

	for _, msg := range errors {
		io.WriteString(out, "\t"+msg+"\n")
	}

	return
}
