# O Language
System Oriented Functional Programming Language (SOFPL)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/olanguage/olang)](https://goreportcard.com/report/gitlab.com/olanguage/olang)

[Documentation](http://onix-project.com/olang/)

[Onix OS Project](http://onix-project.com/)
