package opsfile

import(
  //"fmt"
   "os"
  //"gopkg.in/ini.v1"
  //"github.com/olproject/ops/proc"
  "syscall"
  "gitlab.com/olanguage/olang/pid"
  "log"
  "io/ioutil"
)

func (of *Opsfile) Kill() bool {
  if _, err := os.Stat("pid"); os.IsNotExist(err) {
    log.Printf("Pid folder not found!")
    os.Exit(0)
	}
  
  files, err := ioutil.ReadDir("pid/")
	if err != nil {
		log.Fatal(err)
	}
  
  for _, file := range files {
		id, err := pid.GetValue("pid/"+file.Name())
    if err != nil{
      log.Printf("Process not found.\n")
      os.Exit(1)
    }

    log.Printf("Process stoped: %d\n",id)
    syscall.Kill(id, syscall.SIGTERM)
    os.Remove("pid/"+file.Name())
	}
 
  return true
}