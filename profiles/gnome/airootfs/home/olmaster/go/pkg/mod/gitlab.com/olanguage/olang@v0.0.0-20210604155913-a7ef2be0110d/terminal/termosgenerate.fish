
#set -gx GOOS darwin
#set -gx GOARCH amd64
#go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS darwin
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS openbsd
set -gx GOARCH amd64
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS openbsd
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS freebsd
set -gx GOARCH arm
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS netbsd
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS netbsd
set -gx GOARCH arm
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS netbsd
set -gx GOARCH amd64
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS windows
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS windows
set -gx GOARCH amd64
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS linux
set -gx GOARCH arm
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS linux
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

#set -gx GOOS linux
#set -gx GOARCH amd64
#go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

set -gx GOOS freebsd
set -gx GOARCH 386
go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go

#set -gx GOOS freebsd
#set -gx GOARCH amd64
#go tool cgo -godefs termios.go > termios_(echo $GOOS)_(echo $GOARCH).go