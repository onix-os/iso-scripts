package systemlib

import (
	"io"
	"os"
	"strings"
)

func CreateFile(path string) {
	// detect if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if isError(err) {
			return
		}
		defer file.Close()
	}
}

func WriteFile(path string, content string) bool {
	// open file using READ & WRITE permission
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if isError(err) {
		return false
	}
	defer file.Close()

	content = strings.Replace(content, "\\n", "\n", -1)
	content = strings.Replace(content, "\\r", "\r", -1)
	content = strings.Replace(content, "\\t", "\t", -1)
	content = strings.Replace(content, "\\b", "\b", -1)
	content = strings.Replace(content, "\\\"", "\"", -1)

	cn := []byte(content)

	// write some text line-by-line to file
	_, err = file.Write(cn)

	// save changes
	err = file.Sync()
	if isError(err) {
		return false
	}

	return true
}

func ReadFile(path string) string {
	// re-open file
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if isError(err) {
		return ""
	}
	defer file.Close()

	// read file, line by line
	var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)

		// break if finally arrived at end of file
		if err == io.EOF {
			break
		}

		// break if error occured
		if err != nil && err != io.EOF {
			isError(err)
			break
		}
	}
	return string(text)
}

func DeleteFile(path string) bool {
	// delete file
	var err = os.Remove(path)
	if isError(err) {
		return false
	}
	return true
}

func CreateDir(path string) bool {
	var err = os.MkdirAll(path, 644)
	if isError(err) {
		return false
	}
	return true
}

func DeleteDir(path string) bool {
	var err = os.RemoveAll(path)

	if isError(err) {
		return false
	}
	return true
}

func Link(path1 string, path2 string) bool {
	var err = os.Symlink(path1, path2)

	if isError(err) {
		return false
	}
	return true
}

func isError(err error) bool {
	if err != nil {
		return false
	}

	return (err != nil)
}
