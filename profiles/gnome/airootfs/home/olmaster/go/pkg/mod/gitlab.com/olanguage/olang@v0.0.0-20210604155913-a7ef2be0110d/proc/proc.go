package proc

import(
  "os"
  //"fmt"
	//"bufio"
	"os/exec"
  //"runtime"
  //"syscall"
	"log"
	"gitlab.com/olanguage/olang/pid"
	"io/ioutil"
	"strings"
)

type Proc struct{
	Name string
  Command string
  Arg string
	Process *os.Process
}

func New(name string, command string, arg string) *Proc{
  process := &Proc{
    Command: command,
    Arg: arg,
		Name: name,
  }
  return process
}

func (p *Proc) Execute(){
  if _, err := os.Stat("pid"); os.IsNotExist(err) {
    os.Mkdir("pid",0777)
	}
  cmd := exec.Command("nohup",p.Command, p.Arg,"&")
  cmd.Stdout = os.Stdout
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	pf := pid.New(int64(cmd.Process.Pid))
	pf.Create("pid/"+p.Name+".pid")
	log.Printf("Running Proc: %d\n", cmd.Process.Pid)
	
	res, err := ioutil.ReadFile("nohup.out")
	if err != nil {
		log.Fatal(err)
	}else{
		r := string(res)
		if r != ""{
			output := strings.Replace(r,"\n",",",-2)
			output = strings.Replace(r,"\t",";",-2)
			log.Printf("["+p.Name+"]: %s\n", output)
		}else{
			log.Printf("["+p.Name+"]: No result detected.\n")
		}
	}
	
	os.Remove("nohup.out")
}
