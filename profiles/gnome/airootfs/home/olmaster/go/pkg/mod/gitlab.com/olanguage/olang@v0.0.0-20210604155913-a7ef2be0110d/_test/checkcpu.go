package main

import (
	"gitlab.com/olanguage/olang/cpu"
	"gitlab.com/olanguage/olang/debug"
	"gitlab.com/olanguage/olang/opcode"
)

func main() {
	debug := debug.New()
	cpu := cpu.NewCPU()
	stringPrint := opcode.NewOpcode(byte(opcode.INT_RANDOM))

	bit := make([]byte, 0, 100)
	bit = append(bit, stringPrint.Value())

	cpu.LoadBytes(bit)
	debug.PrintInterface(cpu)
}
