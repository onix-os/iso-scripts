package repl

import (
	"bufio"
	"fmt"
	"io"

	"gitlab.com/olanguage/olang/evaluator"
	"gitlab.com/olanguage/olang/lexer"
	"gitlab.com/olanguage/olang/object"
	"gitlab.com/olanguage/olang/parser"
)

func Start(in io.Reader, out io.Writer, debug bool) {
	fmt.Println("Ctrl-D or quit() function to quit console interpreter.")
	scanner := bufio.NewScanner(in)
	env := object.NewEnvironment()

	for {
		fmt.Printf(PROMPT)
		scanned := scanner.Scan()
		if !scanned {
			return
		}

		line := scanner.Text()
		l := lexer.New(line)
		p := parser.New(l)

		program := p.ParseProgram()
		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			continue
		}

		evaluated := evaluator.Eval(program, env)

		if evaluated != nil {
			if evaluated.Result() != "" {
				io.WriteString(out, evaluated.Result())
				io.WriteString(out, "\n")
			}
		}
	}
}
