#!/bin/bash

latest()
{
    folder=$1
    filename=$2
    if [ ! -z $filename ]
    then
        echo $(ls -tr $folder/$filename* | tail -n 1)
    else
        echo $(ls -tr $folder/* | tail -n 1)
    fi
}

error()
{
    folder=$1
    filename=$2
    cat $(latest $folder $filename) | grep -e error
}


