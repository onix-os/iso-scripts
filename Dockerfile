FROM olproject/onixos:latest

COPY . /root/iso
WORKDIR /root/iso
ENV PATH=$PATH:/usr/bin:/bin
RUN pacman -Scc --noconfirm
RUN pacman -Syu --noconfirm yay-git go git-lfs base-devel arch-install-scripts squashfs-tools dosfstools syslinux git imagemagick archiso --overwrite "*"
CMD [ 'make' 'all' ]
VOLUME ["/root/iso/isofiles"]
