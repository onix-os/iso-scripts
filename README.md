ISO SCRIPTS
-------------

Install;

```
sudo pacman -Syu go base-devel archiso arch-install-scripts squashfs-tools dosfstools syslinux git
```

Compile with;

```
sudo ./build.sh -v -N onix -V 1.0 -L ONIX01 -A Onix -P "Oytun Ozdemir <oytunozdemir@yandex.com>"
```