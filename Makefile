VER:=$(VERSION)
deps:
	/bin/bash ./install-deps.sh


build: ./build-all.sh
	sudo /bin/bash ./build-all.sh

upload:
	/bin/bash upload-all.sh

upload_server:
	/bin/bash upload-server.sh

splash:
	/bin/bash make-splash.sh

clean:
	rm -rf isofiles/*

docker:
	/bin/bash docker-run.sh

all: build
