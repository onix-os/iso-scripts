#!/bin/bash
WORKDIR=$(pwd)

mkdir -p $WORKDIR/isofiles/

list=(gnome xfce)


for item in ${list[@]}
do
	LOG="$WORKDIR/logs/build-$item-$(date +%Y%M%d%H%m).log"
	echo "Building... [$item]"
	cd $WORKDIR/profiles/$item
	sudo /bin/bash ./build.sh &>> $LOG
	cp -av $WORKDIR/profiles/$item/build/out/*.iso $WORKDIR/isofiles/ &>> $LOG
	rm -rf build/
	cd $WORKDIR
done
