#!/bin/bash

WORKDIR=$(pwd)

list=(gnome xfce)


for item in ${list[@]}
do
	cd $WORKDIR/profiles/$item
	convert  -depth 16 -colors 65536 $WORKDIR/graphics/splash/syslinux.png syslinux/splash.png
	cd $WORKDIR
done