#!/bin/bash
export LC_ALL=C
WORKDIR=$(pwd)
SERVER="server.odznames.com"
USER="onix-project"
UPATH="/home/onix-project/domains/pub.onix-project.com/public_html/ISO/Daily/"
PORT="22"

/usr/bin/scp -P $PORT $WORKDIR/isofiles/* $USER@$SERVER:$UPATH
